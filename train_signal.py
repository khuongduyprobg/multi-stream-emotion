import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score, precision_score, recall_score
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.feature_selection import SelectFromModel
from sklearn.utils.class_weight import compute_class_weight
import argparse
import pickle
import config as cf


X_train = pd.read_csv(cf.features_classical_utt_train)
X_test = pd.read_csv(cf.features_classical_utt_test)


y_train = X_train['label']
y_test = X_test['label']
del X_train['wav_file']
del X_train['label']
del X_test['wav_file']
del X_test['label']
X_train = X_train.astype('float32').replace([np.inf, -np.inf], np.nan)
X_test = X_test.astype('float32').replace([np.inf, -np.inf], np.nan)
for col in X_train.columns:
    X_train[col] = X_train[col].fillna(X_train[col].mean())
    X_test[col] = X_test[col].fillna(X_test[col].mean())


emotion_dict = {
    'anger': 0,
    'excited': 1,
    'hapiness': 2,
    'neutral_state': 3,
    'sad': 4
}


def one_hot_encoder(true_labels, num_records, num_classes):
    temp = np.array(true_labels[:num_records])
    true_labels = np.zeros((num_records, num_classes))
    true_labels[np.arange(num_records), temp] = 1
    return true_labels


def display_result(y_test, pred):
    pred = np.argmax(pred, axis=-1)
    one_hot_true = one_hot_encoder(y_test, len(pred), len(emotion_dict))
    print('Test Set Accuracy =  {0:.3f}'.format(accuracy_score(y_test, pred)))
    print('Test Set F-score =  {0:.3f}'.format(f1_score(y_test, pred, average='macro')))
    print('Test Set Precision =  {0:.3f}'.format(precision_score(y_test, pred, average='macro')))
    print('Test Set Recall =  {0:.3f}'.format(recall_score(y_test, pred, average='macro')))


def compute_class_weight(classes):
    # class_weight = compute_class_weight(class_weight='balanced', classes=np.array([0, 1, 2, 3, 4]), y=y_train)
    class_weight_dict = {}
    for i in range(5):
        class_weight_dict[i] = class_weight[i]
    return class_weight_dict


class Model(object):
    rf = RandomForestClassifier(n_estimators=100, min_samples_split=25)
    xgb = xgb.XGBClassifier(max_depth=8, learning_rate=0.001, objective='multi:softprob',
                                 n_estimators=1200, sub_sample=0.8, num_class=len(emotion_dict),
                                 booster='gbtree', n_jobs=4)
    mlp = MLPClassifier(hidden_layer_sizes=(650, ), activation='relu', solver='adam', alpha=0.0001,
                               batch_size='auto', learning_rate='adaptive', learning_rate_init=0.01,
                               power_t=0.5, max_iter=1000, shuffle=True, random_state=None, tol=0.0001,
                               verbose=False, warm_start=True, momentum=0.8, nesterovs_momentum=True,
                               early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999,
                               epsilon=1e-08)
    svc = LinearSVC()
    mnb = MultinomialNB()
    lre = LogisticRegression(solver='lbfgs', multi_class='multinomial', max_iter=1000)
    model_dict = {'rf': rf, 'xgb': xgb, 'mlp': mlp, 'lre': lre}
    
    
    def __init__(self, model):
        self.model_name = model
        self.model = Model.model_dict[model]
    	
    
    def train_model(self, X_train, y_train):	
    	self.model.fit(X_train, y_train)

    
    def save_model(self):
        with open('models/' + self.model_name + '_classifier.pkl', 'wb') as f:
            pickle.dump(self.model, f)

    
    def display_result(self, X_test, y_test):
        pred = self.model.predict_proba(X_test)
        pred = np.argmax(pred, axis=-1)
        one_hot_true = one_hot_encoder(y_test, len(pred), len(emotion_dict))
        print('Test Set Accuracy =  {0:.3f}'.format(accuracy_score(y_test, pred)))
        print('Test Set F-score =  {0:.3f}'.format(f1_score(y_test, pred, average='macro')))
        print('Test Set Precision =  {0:.3f}'.format(precision_score(y_test, pred, average='macro')))
        print('Test Set Recall =  {0:.3f}'.format(recall_score(y_test, pred, average='macro')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Model')

    parser.add_argument('--model',
                        type=str,
                        default='rf')
    
    args = parser.parse_args()
    
    model = Model(args.model)
    
    model.train_model(X_train, y_train)
    
    model.display_result(X_test, y_test)
    
    model.save_model()
