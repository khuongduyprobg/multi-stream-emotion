import os
import pandas as pd
from sklearn.model_selection import train_test_split


wav_files_train, emotions_train, wav_files_test, emotions_test = [], [], [], []
columns = ['wav_file', 'emotion']
df_train = pd.DataFrame(columns=columns)
df_test = pd.DataFrame(columns=columns)
orig_folder = 'data/utterances'


i = 0
for label in os.listdir(orig_folder):
    wav_files_path = os.path.join(orig_folder, label)
    orig_wav_files = os.listdir(wav_files_path)
    audio_train, audio_test = train_test_split(orig_wav_files, test_size=0.2)

    for train_file in audio_train:
        wav_path = os.path.join(wav_files_path, train_file)
        wav_files_train.append(wav_path)
        emotions_train.append(i)

    for test_file in audio_test:
        wav_path = os.path.join(wav_files_path, test_file)
        wav_files_test.append(wav_path)
        emotions_test.append(i)

    i += 1

df_train['wav_file'] = wav_files_train
df_train['emotion'] = emotions_train
df_test['wav_file'] = wav_files_test
df_test['emotion'] = emotions_test


if __name__ == "__main__":
    df_train.to_csv('data/audio_train.csv', index=False)
    df_test.to_csv('data/audio_test.csv', index=False)
