sampling_rate = 16000
duration = 1
n_mels = 128
hop_length = int(125 * duration)  # to make time steps 128
fmin = 20
fmax = sampling_rate // 2
n_mels = 128
n_fft = n_mels * 20

audio_train = 'data/audio_train.csv'
audio_test = 'data/audio_test.csv'
features_classical_train = 'data/features_classical_train.csv'
features_classical_test = 'data/features_classical_test.csv'
features_classical_utt_train = 'data/features_classical_utt_train.csv'
features_classical_utt_test = 'data/features_classical_utt_test.csv'
mels_train = 'data/train_mels.pkl'
mels_tets = 'data/test_mels.pkl'
