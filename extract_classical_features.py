import os
import librosa
import numpy as np
import pandas as pd
import pickle
import config as cf


sequence_length = int(cf.duration * cf.sampling_rate)


def compute_features(signal):
    # Sig mean and std
    sig_mean = np.mean(signal)
    sig_std = np.std(signal)
    # RMSE 
    rmse = librosa.feature.rms(signal + 0.0001)[0]
    rmse_mean = np.mean(rmse)
    rmse_std = np.std(rmse)
    # silence
    silence = 0
    for e in rmse:
        if e <= 0.4 * np.mean(rmse):
            silence += 1
    silence /= float(len(rmse))
    # harmonic
    harmonic, percussive = librosa.effects.hpss(signal)
    harmonic = np.mean(harmonic) * 1000
    # auto_corr
    # autocorr = librosa.core.autocorrelate(signal)
    # auto_corr_max = np.max(autocorr)

    cl = 0.45 * np.mean(abs(signal))
    center_clipped = []
    for s in signal:
        if s >= cl:
            center_clipped.append(s - cl)
        elif s <= -cl:
            center_clipped.append(s + cl)
        elif np.abs(s) < cl:
            center_clipped.append(0)
    auto_corrs = librosa.core.autocorrelate(np.array(center_clipped))
    auto_corr_max = np.max(auto_corrs) * 1000 / len(auto_corrs)
    auto_corr_std = np.std(auto_corrs)
    return sig_mean, sig_std, rmse_mean, rmse_std, silence, harmonic, auto_corr_max, auto_corr_std


def save_features(input_path, output_path, mode):
    in_df = pd.read_csv(input_path)
    columns = ['wav_file', 'label', 'sig_mean', 'sig_std', 'rmse_mean', 'rmse_std', 'silence', 'harmonic', 'auto_corr_max', 'auto_corr_std']
    wav_file , label, sig_mean, sig_std, rmse_mean, rmse_std, silence, harmonic, auto_corr_max, auto_corr_std = [], [], [], [], [], [], [], [], [], []
    feature_dict = {'wav_file': wav_file,
                    'label': label,
                    'sig_mean': sig_mean,
                    'sig_std': sig_std,
                    'rmse_mean': rmse_mean,
                    'rmse_std': rmse_std,
                    'silence': silence,
                    'harmonic': harmonic,
                    'auto_corr_max': auto_corr_max,
                    'auto_corr_std': auto_corr_std
                   }
    out_df = pd.DataFrame(columns=columns)

    audios = in_df['wav_file'].tolist()
    labels = in_df['emotion'].tolist()
    for i in range(len(audios)):
        orig_wav_vector, _sr = librosa.load(audios[i], sr=cf.sampling_rate)
        if mode == 'segment':
            for index in range(0, orig_wav_vector.shape[0], sequence_length):
                end = min(orig_wav_vector.shape[0], index + sequence_length)
                segment = orig_wav_vector[index:end]
                _sig_mean, _sig_std, _rmse_mean, _rmse_std, _silence, _harmonic, _auto_corr_max, _auto_corr_std = compute_features(segment)
                wav_file.append(audios[i])
                label.append(labels[i])
                sig_mean.append(_sig_mean)
                sig_std.append(_sig_std)
                rmse_mean.append(_rmse_mean)
                rmse_std.append(_rmse_std)
                silence.append(_silence)
                harmonic.append(_harmonic)
                auto_corr_max.append(_auto_corr_max)
                auto_corr_std.append(_auto_corr_std)
        else:
            utt = orig_wav_vector
            _sig_mean, _sig_std, _rmse_mean, _rmse_std, _silence, _harmonic, _auto_corr_max, _auto_corr_std = compute_features(utt)
            wav_file.append(audios[i])
            label.append(labels[i])
            sig_mean.append(_sig_mean)
            sig_std.append(_sig_std)
            rmse_mean.append(_rmse_mean)
            rmse_std.append(_rmse_std)
            silence.append(_silence)
            harmonic.append(_harmonic)
            auto_corr_max.append(_auto_corr_max)
            auto_corr_std.append(_auto_corr_std)

    for feature in columns:
        out_df[feature] = feature_dict[feature]
    out_df.to_csv(output_path, index=False)


if __name__ == '__main__':
    save_features(cf.audio_train, cf.features_classical_utt_train, mode='utt')
    save_features(cf.audio_test, cf.features_classical_utt_test, mode='utt')
