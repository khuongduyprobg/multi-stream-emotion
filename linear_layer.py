import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas as pd
import config as cf
from sklearn.metrics import accuracy_score
import argparse


class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(8, 7)
        self.fc2 = nn.Linear(7, 6)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


def read_data(input_path):
    df = pd.read_csv(input_path)
    labels = np.array(df['label'].fillna(0))
    del df['wav_file']
    del df['label']
    data = np.array(df)
    return data, labels


def train(args):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    model = Net().to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)
    for epoch in range(args.epochs):
        model.train()
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)

            model.zero_grad()
            optimizer.zero_grad()

            predictions = model(inputs)
            predictions = predictions.to(device)

            loss = criterion(predictions, labels)
            loss.backward()
            optimizer.step()
            print(loss.item())
        # Evaluate
        acc = 0
        with torch.no_grad():
            for i, data in enumerate(test_loader, 0):
                inputs, labels = data
                inputs = inputs.to(device)
                labels = labels.to(device)
                predictions = model(inputs)
                # acc += accuracy_score(predictions.cpu().numpy(), labels.cpu().numpy())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--epochs", type=int, default=100)
    parser.add_argument("--lr", type=float, default=1e-6)
    args = parser.parse_args()

    # print(read_data(cf.features_classical_train))
    X_train, y_train = read_data(cf.features_classical_train)
    X_test, y_test = read_data(cf.features_classical_test)
    X_train = torch.from_numpy(X_train).float()
    y_train = torch.from_numpy(y_train).long()
    X_test = torch.from_numpy(X_test).float()
    y_test = torch.from_numpy(y_test).long()
    train_data = torch.utils.data.TensorDataset(X_train, y_train)
    test_data = torch.utils.data.TensorDataset(X_test, y_test)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=64, shuffle=False)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=64, shuffle=False)

    train(args)
