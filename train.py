import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torchvision.transforms import transforms

import numpy as np
import pandas as pd
import argparse
import pickle

from cnn_classifier import Classifier
import config as cf
from extract_mel_spectrogram import read_binary
from utils import Dataset_Loader


transforms_dict = {
    'train': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
    'test': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
}


def prepare_data(input_path, mode='train'):
    with open(input_path, 'rb') as f:
        temp = pickle.load(f)
    X, y = temp[0], temp[1]
    dataset = Dataset_Loader(X, y, transforms=transforms_dict[mode])
    # X = torch.tensor(X, dtype=torch.float32)
    # y = torch.tensor(y, dtype=torch.long)
    # X = torch.from_numpy(X)
    # y = torch.from_numpy(y)
    # data = torch.utils.data.TensorDataset(X, y)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=64, shuffle=False)
    return dataloader


def train_model(args):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    model = Classifier(num_classes=5).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(params=model.parameters(), lr=args.lr, amsgrad=False)
    performance = 0

    for epoch in range(args.epochs):
        print(f"Training epoch:{epoch}")
        model.train()
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)

            model.zero_grad()
            optimizer.zero_grad()

            predictions = model(inputs)
            predictions = predictions.to(device)

            loss = criterion(predictions, labels)
            loss.backward()
            optimizer.step()
        # Evaluate
        correct = 0
        total = 0
        with torch.no_grad():
            for data in test_loader:
                inputs, labels = data
                inputs = inputs.to(device)
                labels = labels.to(device)
                outputs = model(inputs)
                _, predictions = torch.max(outputs.data, dim=1)
                total += labels.size(0)
                correct += (predictions == labels).sum().item()
            if correct / total > performance:
                performance = correct / total
                print(f"Best accuracy: {performance}")
                torch.save(model.state_dict(), 'models/best_model.pkl')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=1000)
    parser.add_argument('--lr', type=float, default=1e-6)
    args = parser.parse_args()

    train_loader = prepare_data(cf.mels_train, mode='train')
    test_loader = prepare_data(cf.mels_tets, mode='test')

    train_model(args)
