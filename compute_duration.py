import wave
import contextlib
import os


def duration(file_path):
    with contextlib.closing(wave.open(file_path, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        return duration


if __name__ == '__main__':
    total_duration = 0
    folder_path = 'utterances'
    list_inside = os.listdir(folder_path)
    for folder in list_inside:
        path = folder_path + '/' + folder
        list_file = os.listdir(path)
        for f in list_file:
            file_path = path + '/' + f
            total_duration += duration(file_path)
    print('Total time in seconds: {}'.format(total_duration))
    print('Total time in hours: {}'.format(total_duration / 3600))

    file_path = 'utterances/0/Ses01F_impro01_M011.wav'
    print('Time in seconds: {}'.format(duration(file_path)))
